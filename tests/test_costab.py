import numpy as np
import costab
import glob
import os

PWD = os.path.join(os.path.dirname(__file__))


def test_load_costab():
    samples = glob.glob(os.path.join(PWD, "data/*.xls*"))
    assert len(samples) > 0
    for sample in samples:
        excel = costab.load(sample)
        assert len(excel.sheet_names) > 0


def test_cost_sheet_names():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    cost_sheets = costab.cost_sheet_names(excel)
    expected = [
        "DT_1_1", "DT_2_1",
        "DT_3_1", "DT_3_2", "DT_3_3", "DT_3_4", "DT_3_5",
        "DT_4_1", "DT_4_2", "DT_4_3"
    ]
    assert sorted(cost_sheets) == expected


def test_df_from_name():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    names = costab.cost_sheet_names(excel)
    df = costab.df_from_name(names[0], excel)
    assert len(df) > 0


def test_first_valid_row():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    assert costab.first_valid_row(df) == 3


def test_base_costs_column():
    pass


def test_includingcontingencies_column():
    pass


def test_get_currency():
    pass


def test_investmentcosts_rows():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    assert costab.investmentcosts_rows(df) == ((6, 1),  (24, 1))


def test_get_investment_items():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    rows = costab.investmentcosts_rows(df)
    records = costab.get_records(df, rows)
    expected = [
        [
            [
                'A. Construction Cost',
                '1. Upazila Roads',
                'a. Roads'
            ],
            ["km", 24711999.325619366, 130]
        ],
        [
            [
                'A. Construction Cost',
                '1. Upazila Roads',
                'b. Structures'
            ],
            ["m", 6354514.112302124, 780]
            ],
        [
            [
                'A. Construction Cost',
                '1. Upazila Roads',
                'c. Drainage Improvement'
            ],
            ["km", 2551416.163262195, 270]
        ],
        [
            [
                'A. Construction Cost',
                '2. Union, Village and Block Roads',
                "a. Union Roads BC type 'B'"],
            ["km", 15609068.879260056, 128]
        ],
        [
            [
                'A. Construction Cost',
                '2. Union, Village and Block Roads',
                "b. Union Roads BC type 'C'"
            ],
            ["km", 4547460.941725037, 32]
        ],
        [
            [
                'A. Construction Cost',
                '2. Union, Village and Block Roads',
                'c. Village Roads BC'
            ],
            ["km", 22031094.463829264, 271]
        ],
        [
            [
                'A. Construction Cost',
                '2. Union, Village and Block Roads',
                'd. Village Roads RCC'],
            ["km", 6428127.1339312475, 65]
        ],
        [
            [
                'A. Construction Cost',
                '2. Union, Village and Block Roads',
                'e. Block Roads'],
            ["km", 324762.83803646336, 5]
        ],
        [
            [
                'A. Construction Cost',
                '2. Union, Village and Block Roads',
                'f. Structures',
                'Bridges'
            ],
            ["m", 745921.6372454267, 110]
        ],
        [
            [
                'A. Construction Cost',
                '2. Union, Village and Block Roads',
                'f. Structures',
                'Culverts and Small Bridges'
            ],
            ["m", 9570641.415768377, 2345]
        ]
    ]
    assert records == expected


def test_jsonify_records():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    rows = costab.investmentcosts_rows(df)
    records = costab.get_records(df, rows)
    jsonified = costab.jsonify_records(records)
    expected = {
        "activity": 'A. Construction Cost',
        "item": '1. Upazila Roads a. Roads',
        "unit": "km",
        "costs": 24711999.325619366,
        "quantities": 130
    }
    assert jsonified[0] == expected


def test_recurrentcosts_rows():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    assert costab.recurrentcosts_rows(df) == ((25, 1), (len(df), 1))


def test_get_recurrent_items():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    rows = costab.recurrentcosts_rows(df)
    records = costab.get_records(df, rows)
    expected = [
        [
            [
                'A. Maintenance Cost /a',
                '1. Upazila/Union Roads'
            ],
            ['sum', 1291527.9638217213, np.nan]],
        [
            [
                'A. Maintenance Cost /a',
                '2. Union, Village and Block Roads'
            ],
            ['sum', 2864049.917470107, np.nan]
        ]
    ]

    assert records == expected
