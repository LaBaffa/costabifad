import extract
import os
import costab

PWD = os.path.join(os.path.dirname(__file__))


def test_items_from_df():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    recurrent_rows = costab.recurrentcosts_rows(df)
    investment_rows = costab.investmentcosts_rows(df)
    investment_records = costab.get_records(df, investment_rows)
    recurrent_records = costab.get_records(df, recurrent_rows)
    records = costab.jsonify_records(
        investment_records + recurrent_records
    )
    out = extract.items_from_df(df)
    assert all(
        all(item in o.items() for item in r.items())
        for r, o in zip(records, out)
        )


def test_extract():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    out = extract.extract(sample)
    assert out[0] == {
        'unit': 'km',
        'project_number': '1647',
        'costs': 24711999.325619366,
        'component': None,
        'activity': 'A. Construction Cost',
        'item': '1. Upazila Roads a. Roads',
        'currency': 'us$',
        'component_number': '1.1',
        'subcomponent': '1.1. upgraded rural roads',
        'currency_unit': 1,
        "quantities": 130,
        "country": "Bangladesh"
    }

