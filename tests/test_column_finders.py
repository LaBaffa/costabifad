import column_finders as cf
import os
import costab


PWD = os.path.join(os.path.dirname(__file__))


def test_reference_columns():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    # first_valid_row = costab.first_valid_row(df)
    col_numbers = cf.reference_columns(df)
    expected = {
        "totals including contingencies": [26],
        "base cost": [19],
        "unit": [7],
        "unit cost": [15],
        "total": [0, 14, 22, 29, 33, 49, 56, 63, 70, 77],
        "quantities": [11]
    }
    assert col_numbers == expected


def test_unit_column():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    first = costab.first_valid_row(df)
    cols = cf.reference_columns(df[first:])
    assert cf.unit_column(cols) == 7


def test_costs_column():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    first = costab.first_valid_row(df)
    cols = cf.reference_columns(df[first:])
    costs_col = cf.costs_column(cols)
    # print(costs_col, df[costs_col])
    assert costs_col == 29


# def test_find_substrings_in_columns():
#     sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
#     excel = costab.load(sample)
#     df = costab.df_from_name("DT_1_1", excel)
#     first = costab.first_valid_row(df)
#     cols = cf.reference_columns(df[first:])
#     doubles = cf.find_substrings_in_columns(cols)
#     assert doubles == {}


# def test_filter_doubles_in_columns():
#     sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
#     excel = costab.load(sample)
#     df = costab.df_from_name("DT_1_1", excel)
#     first = costab.first_valid_row(df)
#     cols = cf.reference_columns(df[first:])
#     filtered = cf.filter_doubles_in_columns(cols)
#     expected = {
#         'quantities': [11],
#         'unit': [7],
#         'totals including contingencies': [26],
#         'base cost': [19],
#         'total': [0, 1, 3, 4, 14, 22, 26, 29, 33, 49, 56, 63, 70, 77],
#         'unit cost': [15]
#     }
#     assert filtered == expected


def test_subcomponent():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    assert cf.subcomponent(df) == "1.1. upgraded rural roads"


def test_component_number():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    names = costab.cost_sheet_names(excel)
    component_numbers = [cf.component_number(n) for n in names]
    expected = [
        "1.1",
        "2.1",
        "3.1", "3.2", "3.3", "3.4", "3.5",
        "4.1", "4.2", "4.3"
    ]
    assert component_numbers == expected
    

def test_proj_number():
    assert cf.proj_number("MDG_2_0119_2015_COSTAB.xlsx") == "0119"
    assert cf.proj_number("MDG_2_2015_2019_COSTAB.xlsx") == "2019"
    assert cf.proj_number("MDG_2_2016_COSTAB_2015.xlsx") is None


def test_country_candidates():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    out = cf.country_candidates(df)
    assert out == ["Bangladesh"]
    

def test_base_cost_pos():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    df_after = df[costab.first_valid_row(df):]
    out = cf.base_cost_pos(df_after)
    assert out == (3, 19)


def test_detailed_costs_pos():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    out = cf.detailed_costs_pos(df)
    assert out == (3, 0)


def test_currency_candidates():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    base_pos = cf.base_cost_pos(df)
    detailed_pos = cf.detailed_costs_pos(df)
    out = cf.currency_candidates(df, [base_pos, detailed_pos])
    expected = [
        'Base Cost (US$)', '2016',
        "Detailed Costs"
    ]
    assert out == expected
    

def test_currency_and_unit():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    base_pos = cf.base_cost_pos(df)
    detailed_pos = cf.detailed_costs_pos(df)
    cand = cf.currency_candidates(df, [base_pos, detailed_pos])
    out = cf.currency_and_unit(cand)
    expected = ("us$", 1)
    assert out == expected


def test_quantities_column():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    first = costab.first_valid_row(df)
    cols = cf.reference_columns(df[first:])
    out = cf.quantities_column(cols)
    assert out == 14


def test_investmentcosts_pos():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    out = cf.investmentcosts_pos(df)
    assert out == (6, 1)

    
def test_recurrentcosts_pos():
    sample = os.path.join(PWD, "data/BDG_1647_2013_COSTAB.xlsx")
    excel = costab.load(sample)
    df = costab.df_from_name("DT_1_1", excel)
    out = cf.recurrentcosts_pos(df)
    assert out == (25, 1)
