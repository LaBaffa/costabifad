from costasearch.util.result_parser import jsonify_tupled_multindex
from costaparse.settings.columns import PROJECT_NUMBER


def get_project_numbers_in_df(df):
    """
    Get all project numbers present in a given costab df

    Params
    ------
    df (pandas.DataFrame) -- dataframe from costaparse with total columns

    """
    projects = df.groupby(PROJECT_NUMBER)
    return list(projects.groups.keys())
    

def get_project_items_in_df(df, project_number):
    """ Get all items in df related to project_number"""
    return df.groupby(PROJECT_NUMBER).get_group(project_number)


def dfs_by_project(df):
    """Return list of df divided by project"""
    
    groups = df.groupby(PROJECT_NUMBER)
    projects = list(groups.groups.keys())
    return [
        groups.get_group(n)
        for n in projects
    ]


def is_project_in_db(proj_number, collection):
    """ Search for a project in a given collection of items"""
    res = collection.find_one({PROJECT_NUMBER: proj_number})
    return False if res is None else True


def insert_project_to_mongo(proj_df, collection, do_update=False):
    """
    Insert items of proj_df on database.
    If do_update is True, susbstitute old items, if present.

    Params
    ------
    prof_df (pd.DataFrame) -- dataframe related to a given project's items
                              (df_with_totals.groupby("project_number").get_group(ID))

    collection (pymongo.collection) -- collection of items in mongodb
    """
    proj_items = [
        jsonify_tupled_multindex(x)
        for x in proj_df.to_dict("records")
    ]
    sample = proj_items[0]
    n = sample[PROJECT_NUMBER]
    if is_project_in_db(n, collection):
        if do_update:
            print(
                "Project {} already in database. Substituting items".format(
                    n
                ))
            collection.delete_many({PROJECT_NUMBER: n})
            for x in proj_items:
                collection.insert_one(x)
        else:
            print("Project {} already in database. Skipping".format(
                n
            ))
    else:
        print("Inserting new project {}".format(n))
        for x in proj_items:
            collection.insert_one(x)
