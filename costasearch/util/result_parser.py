import pandas as pd
from costaparse.settings.config import UNIT_STRINGS
from costasearch.settings import outputs


DOLLARS = ["usd", "$", "us$", "usd$"]
tot_cols = ["totals including contingencies", "base cost"]
ifad_cols = ["ifad", "ifad_loan", "ifad_grant"]


def curr_unit(r, e):
    if e is not None:
        if pd.notnull(e["unit"]):
            unit = e["unit"]
        else:
            if pd.isnull(r["sheet_currency_unit"].values[0]):
                unit = "1"
            else:
                unit = r["sheet_currency_unit"].values[0]
        return unit
    

def total_and_ifad(r):
    tot = next(
        (
            r[c]
            for c in tot_cols
            if pd.notnull(r[c]["value"]) and (
                    r[c]["currency"] in DOLLARS or (
                        pd.isnull(r[c]["currency"]) and
                        r["sheet_currency"].values[0] in DOLLARS
                    )
            )
        ), None
    )
    notnan_ifad = [
        c
        for c in ifad_cols
        if pd.notnull(r[c]["value"]) and (
                (r[c]["currency"] in DOLLARS) or
                (
                    pd.isnull(r[c]["currency"]) and
                    (r["sheet_currency"].values[0] in DOLLARS)
                )
        )
    ]

    ifad = r[notnan_ifad].sum(level=1)["value"] if notnan_ifad else 0
    tot_unit = curr_unit(r, tot)
    ifad_e = r[notnan_ifad[0]] if notnan_ifad else None
    ifad_unit = curr_unit(r, ifad_e)
    return {
        "total": tot["value"] if tot is not None else 0,
        "total unit": tot_unit if tot is not None else "million",
        "ifad": ifad,
        "ifad unit": ifad_unit if ifad else "million"
    }


def dollarcosts_by_item(df):
    return df.apply(total_and_ifad, axis=1)


def sums_in_unit(out, unit="million"):
    unit_numeric = UNIT_STRINGS[unit]
    out_df = pd.DataFrame(out.tolist())
    out_df["ifad unit"] = out_df["ifad unit"].apply(
        lambda x: UNIT_STRINGS[str(x)])
    out_df["total unit"] = out_df["total unit"].apply(
        lambda x: UNIT_STRINGS[str(x)])
    out_df["ifad"] = out_df["ifad"] * (out_df["ifad unit"]/unit_numeric)
    out_df["total"] = out_df["total"] * (out_df["total unit"]/unit_numeric)
    sums = {
        outputs.IFAD_COSTS: out_df["ifad"].sum(),
        outputs.COSTS_SUM: out_df["total"].sum(),
        outputs.CURRENCY: "USD",
        outputs.UNITS: unit
    }
    return sums


def jsonify_tupled_multindex(a):
    """
    a is dict with keys of the form (outer, inner),
    e.g {(a, b): 1, (a, c): 2, (d, ""): 3}
    a is a record coming  from a multiindexed dataframe
    with df.to_dict("records")

    Return:
    {a: {b: 1, c:2}, d: 3}

    """
    o = {}
    for (outer, inner), v in a.items():
        if inner:
            o.setdefault(outer, {})
            o[outer][inner] = v
        else:
            o[outer] = v
    return o


