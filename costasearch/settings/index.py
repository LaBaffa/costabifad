from whoosh import fields


SCHEMA = fields.Schema(
    idx=fields.ID(stored=True),
    body=fields.TEXT(stored=True)
)
BODY_COLUMN = "all_text"

SCHEMA_MONGO = fields.Schema(
    idx=fields.ID(stored=True),
    activity=fields.TEXT(stored=True),
    item=fields.TEXT(stored=True),
    # body=fields.TEXT(stored=True)
)
SCHEMA_MONGO_BIS = fields.Schema(
    idx=fields.ID(stored=True),
    activity=fields.TEXT(stored=True),
    item=fields.TEXT(stored=True)
)
