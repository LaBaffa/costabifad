from costaparse.settings.config import MULTI_INDEX

PROJECT_COUNT = "Project count"
COUNTRY_COUNT = "Country count"
ITEM_COUNT = "Item count"
COSTS_SUM = "Total"
IFAD_COSTS = "IFAD"
ITEM_COST = "mean cost per item"
IFAD_ITEM_COST = "mean ifad cost per item"
CURRENCY = "Currency"
UNITS = "Unit"
PROJECT_IDS = "Project ID"
COUNTRIES = "Country"
ACTIVITY = "Activity"
FROM = "From"
TO = "To"
ITEM = "Item"
SHEET = "Sheet"

COSTS_TABLE_COLUMNS = [
    ITEM_COUNT,
    PROJECT_COUNT,
    COUNTRY_COUNT,
    CURRENCY,
    UNITS,
    COSTS_SUM,
    IFAD_COSTS
]

DETAILS_TABLE_COLUMNS = [
    PROJECT_IDS,
    COUNTRIES,
    ACTIVITY,
    ITEM,
    FROM,
    TO,
    COSTS_SUM,
    IFAD_COSTS,
    SHEET
]
PROJECTS_TABLE_COLUMNS = [
    PROJECT_IDS,
    COUNTRIES,
    FROM,
    TO,
    CURRENCY,
    UNITS,
    COSTS_SUM,
    IFAD_COSTS
]
COUNTRIES_TABLE_COLUMNS = [
    COUNTRIES,
    PROJECT_COUNT,
    FROM,
    TO,
    CURRENCY,
    UNITS,
    COSTS_SUM,
    IFAD_COSTS
]
TOTAL_USD_MILLION = "total USD million"
IFAD_USD_MILLION = "ifad USD million"
MULTI_INDEX_WITH_TOTALS = MULTI_INDEX + [
    (TOTAL_USD_MILLION, ""), (IFAD_USD_MILLION, "")]
