from whoosh import qparser, query
from costasearch.settings.index import SCHEMA, SCHEMA_MONGO


OG = qparser.OrGroup.factory(0.9)
PARSER = qparser.MultifieldParser(
    ["body"],
    SCHEMA,
    group=OG,
    termclass=query.Variations
)
MONGO_PARSER = qparser.MultifieldParser(
    ["activity", "item"],
    SCHEMA_MONGO,
    group=OG,
    termclass=query.Variations
)
