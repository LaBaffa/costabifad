import pandas as pd
from costasearch.settings.outputs import MULTI_INDEX_WITH_TOTALS
import click
from costasearch.util.mongo import insert_project_to_mongo
from costaparse.settings.columns import PROJECT_NUMBER
import pymongo
from costasearch.settings.db import COSTAB_DB, MLAB_STRING


@click.command()
@click.argument(
    "source",
    metavar="SOURCE",
    type=click.Path()
)
@click.option("--update", is_flag=True)
def main(source, update):
    client = pymongo.MongoClient(MLAB_STRING)
    collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]

    excel = pd.ExcelFile(source)
    df = excel.parse(excel.sheet_names[0], header=[0, 1], index_col=0)
    cols = pd.MultiIndex.from_tuples(MULTI_INDEX_WITH_TOTALS)
    df.columns = cols
    groups = df.groupby(PROJECT_NUMBER)
    projects = list(groups.groups.keys())
    for n in projects:
        proj_df = groups.get_group(n)
        insert_project_to_mongo(proj_df, collection, do_update=update)

