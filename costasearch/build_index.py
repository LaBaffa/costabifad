import os
from whoosh import index
import pandas as pd
import click
from costasearch.settings.outputs import MULTI_INDEX_WITH_TOTALS
from costasearch.settings.index import SCHEMA, BODY_COLUMN


def load(dataset, cols=MULTI_INDEX_WITH_TOTALS):
    xls_obj = pd.ExcelFile(dataset)
    df = xls_obj.parse(
        xls_obj.sheet_names[0], header=[0, 1], index_col=0
    )
    cols = pd.MultiIndex.from_tuples(cols)
    df.columns = cols
    return df


def populate_index(dirname, dataframe, schema):
    if not os.path.exists(dirname):
        os.mkdir(dirname)
    ix = index.create_in(dirname, schema)
    with ix.writer() as writer:
        for i in dataframe.index:
            add_item(i, dataframe, writer)
    

def add_item(i, dataframe, writer):
    writer.update_document(
        idx=str(i),
        body=str(dataframe.loc[i, BODY_COLUMN].values[0])
    )
        

@click.command()
@click.argument(
    "path",
    metavar="INPUT",
    type=click.Path(exists=True)
)
@click.option(
    "--outdir", "-o",
    metavar="OUTPUT",
    type=click.Path(),
    default="db_index")
def main(path, outdir):
    print("loading dataset from {}".format(path))
    df = load(path)
    df[BODY_COLUMN] = df["subcomponent"] + df["activity"] + df["item"]
    print("populating index")
    populate_index(outdir, df, SCHEMA)
    print("added index in {}".format(outdir))


if __name__ == "__main__":
    main()
