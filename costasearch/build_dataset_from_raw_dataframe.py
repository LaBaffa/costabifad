"""
This script parse outputs from costaparse/jsonrecords_to_csv.py in order to put
it into a suitable shape.

1) Add columns for totals and ifad costs in USD
2) Translate text from languages other than english
3) Check for duplicates (?)

"""
from costasearch.settings import outputs
from costasearch.util.result_parser import total_and_ifad
from costaparse.settings.config import MULTI_INDEX, UNIT_STRINGS
import click
import pandas as pd


@click.command()
@click.argument(
    "data_path",
    metavar="SOURCE",
    type=click.STRING
)
@click.option(
    "--out_path", "-o",
    metavar="OUTPUT",
    type=click.Path(),
    default="dataset.xlsx"
)
def main(data_path, out_path):
    excel = pd.ExcelFile(data_path)
    df = excel.parse(excel.sheet_names[0], header=[0, 1], index_col=0)
    cols = pd.MultiIndex.from_tuples(MULTI_INDEX)
    df.columns = cols
    out = df.apply(total_and_ifad, axis=1)
    t_i_million = []
    for i, x in enumerate(out):
        tu = UNIT_STRINGS[str(x["total unit"])]
        iu = UNIT_STRINGS[str(x["ifad unit"])]
        try:
            o = {
                "total": float(x["total"])*(tu/1e6),
                "ifad": float(x["ifad"])*(iu/1e6)
            }
        except ValueError:
            o = dicpt.fromkeys(["total", "ifad"])
        t_i_million.append(o)
    df[outputs.TOTAL_USD_MILLION] = [x["total"] for x in t_i_million]
    df[outputs.IFAD_USD_MILLION] = [x["ifad"] for x in t_i_million]
    writer = pd.ExcelWriter(out_path)
    df.to_excel(writer)
    writer.save()
    writer.close()


if __name__ == "__main__":
    main()
