import pymongo
from whoosh import index
import click
from costasearch.settings.index import SCHEMA_MONGO
from costasearch.settings.db import MLAB_STRING, COSTAB_DB
import os


def populate_index(dirname, collection, schema):
    if not os.path.exists(dirname):
        os.mkdir(dirname)
    ix = index.create_in(dirname, schema)
    with ix.writer() as writer:
        for record in collection.find():
            try:
                add_item(record, writer)
            except ValueError as e:
                print(record)
                raise e
    

def add_item(record, writer):
    activity = (
        record["activity"] if isinstance(record["activity"], str) else ""
        )
    item = record["item"] if isinstance(record["item"], str) else ""
    subcomponent = (
        record["subcomponent"]
        if isinstance(record["subcomponent"], str) else ""
        )
    writer.update_document(
        idx=str(record["_id"]),
        activity=activity,
        item=item,
        body=" ".join([activity, item])
    )
        

@click.command()
@click.argument(
    "index_dir",
    metavar="INDEX_DIR",
    type=click.Path(),
    default="db_index")
def main(index_dir):
    print("connecting to db")
    client = pymongo.MongoClient(MLAB_STRING)
    collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]
    print("populating index")
    populate_index(index_dir, collection, SCHEMA_MONGO)
    print("added index in {}".format(index_dir))

    
if __name__ == "__main__":
    main()
