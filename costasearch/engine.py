from shapely.geometry import shape, MultiPoint
import folium
import json
from costasearch.settings.searcher import MONGO_PARSER
import pandas as pd
from whoosh import index
import click
from costaparse.settings import columns
from costasearch.settings import outputs
from costasearch.settings.db import MLAB_STRING, COSTAB_DB
import pymongo
from bson.objectid import ObjectId
from pathlib import Path

DATASET_PATH = "/home/gio/Downloads/costabs/dataset_with_totals.xlsx"
WORLD_GEO_PATH = str(
    Path(__file__).resolve().parent.parent
    / 'data'
    / 'world-countries.json'
)


def nan_to_invalid_years(f, t):
    if any(x is None or x > 2050 for x in (f, t)):
        return None, None
    try:
        f, t = int(f), int(t)
    except ValueError:
        f, t = None, None
    return f, t


class Engine():
    def __init__(self, index_path):
        self.index_path = index_path
        self.results = []
        self.results_df = None

    def search(self, search_query, engine_parser=MONGO_PARSER, limit=None):
        ix = index.open_dir(self.index_path)
        q = engine_parser.parse(search_query)
        outs = []
        with ix.searcher() as s:
            results = s.search(q, limit=limit)
            for hit in results:
                # r = dict(hit)
                # r.update({"highlights": hit.highlights("body")})
                outs.append(dict(hit.items()))
                
        self.results = outs
        return outs

    def get_results_from_mongo(self):
        """ Warning: use this method if you know data are not big"""
        idx = [ObjectId(x["idx"]) for x in self.results]
        client = pymongo.MongoClient(MLAB_STRING)
        collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]
        data = list(collection.find({"_id": {"$in": idx}}))  # here careful
        self.results_df = pd.DataFrame(data)
        return data
                    
    def search_data(self):
        idx = [int(x["idx"]) for x in self.results]
        return self.df.loc[idx]

    @staticmethod
    def null_output(cols=outputs.COSTS_TABLE_COLUMNS):
        return dict.fromkeys(cols)
        
    @staticmethod
    def costs(df):
        if len(df) == 0:
            return Engine.null_output()
        totals = df[columns.CONTINGENCIES]["value"].sum()
        ifad = df[columns.IFAD]["value"].sum()
        totals_mean = totals / len(df)
        ifad_mean = ifad / len(df)
        return {
            outputs.COSTS_SUM: round(totals, 2),
            outputs.ITEM_COST: round(totals_mean, 2),
            outputs.IFAD_COSTS: round(ifad, 2),
            outputs.IFAD_ITEM_COST: round(ifad_mean, 2),
            outputs.CURRENCY: "USD",
            outputs.UNITS: "'000"
        }

    @staticmethod
    def general_info(df):
        if len(df) == 0:
            return Engine.null_output()
        projects = df.drop_duplicates(("project_number", ""))
        project_count = len(projects)
        countries = projects["country"].unique()
        country_count = len(countries)
        return {
            outputs.ITEM_COUNT: len(df),
            outputs.PROJECT_COUNT: project_count,
            outputs.COUNTRY_COUNT: country_count
        }
    
    @staticmethod
    def breakdown_dollarcosts(df, unit="million"):
        if len(df) == 0:
            return Engine.null_output()
        total = df["total USD million"].sum()
        ifad = df["ifad USD million"].sum()
        return {
            outputs.COSTS_SUM: round(total, 2),
            outputs.IFAD_COSTS: round(ifad, 2),
            outputs.CURRENCY: "USD",
            outputs.UNITS: unit
        }

    def general_info_from_mongo(self):
        if not self.results:
            return Engine.null_output()
        idx = [ObjectId(x["idx"]) for x in self.results]
        client = pymongo.MongoClient(MLAB_STRING)
        collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]
        cursor = collection.find({"_id": {"$in": idx}})
        project_ids = cursor.distinct("project_number")
        project_count = len(project_ids)
        countries = cursor.distinct("country")
        country_count = len(countries)
        return {
            outputs.PROJECT_IDS: project_ids,
            outputs.COUNTRIES: countries,
            outputs.ITEM_COUNT: len(idx),
            outputs.PROJECT_COUNT: project_count,
            outputs.COUNTRY_COUNT: country_count
        }

    def infos_by_project_from_mongo(self):
        if not self.results:
            return Engine.null_output()
        idx = [ObjectId(x["idx"]) for x in self.results]
        client = pymongo.MongoClient(MLAB_STRING)
        collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]
        cursor = collection.find({"_id": {"$in": idx}})
        results = []
        for record in cursor:
            f, t = nan_to_invalid_years(
                record[columns.FROM_YEAR], record[columns.TO_YEAR])
            info = {
                outputs.PROJECT_IDS: record[columns.PROJECT_NUMBER],
                outputs.COUNTRIES: record[columns.COUNTRY],
                outputs.ACTIVITY: record[columns.ACTIVITY],
                outputs.ITEM: record[columns.ITEM],
                outputs.FROM: int(f or None) if f else None,
                outputs.TO: int(t or None) if t else None,
                outputs.COSTS_SUM: round(record[outputs.TOTAL_USD_MILLION], 4),
                outputs.IFAD_COSTS: round(record[outputs.IFAD_USD_MILLION], 4),
                outputs.SHEET: record[columns.SHEET]
            }
            results.append(info)
        return results
        
    def breakdown_dollarcosts_from_mongo(self, unit="million"):
        if not self.results:
            return Engine.null_output()
        idx = [ObjectId(x["idx"]) for x in self.results]
        client = pymongo.MongoClient(MLAB_STRING)
        collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]
        agg = collection.aggregate(
            [
                {"$match": {"_id": {"$in": idx}}},
                {
                    "$group": {
                        "_id": None,
                        outputs.TOTAL_USD_MILLION: {
                            "$sum": "${}".format(outputs.TOTAL_USD_MILLION)},
                        outputs.IFAD_USD_MILLION: {
                            "$sum": "${}".format(outputs.IFAD_USD_MILLION)}
                    }}]
        )
        results = list(agg)[0]
        return {
            outputs.COSTS_SUM: round(results[outputs.TOTAL_USD_MILLION], 2),
            outputs.IFAD_COSTS: round(results[outputs.IFAD_USD_MILLION], 2),
            outputs.CURRENCY: "USD",
            outputs.UNITS: unit
        }

    def breakdown_by_project_from_mongo(self, unit="million"):
        if not self.results:
            return Engine.null_output()
        idx = [ObjectId(x["idx"]) for x in self.results]
        client = pymongo.MongoClient(MLAB_STRING)
        collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]
        agg = collection.aggregate(
            [
                {"$match": {"_id": {"$in": idx}}},
                {
                    "$group": {
                        "_id": {
                            outputs.PROJECT_IDS: "${}".format(
                                columns.PROJECT_NUMBER)},
                        outputs.TOTAL_USD_MILLION: {
                            "$sum": "${}".format(outputs.TOTAL_USD_MILLION)},
                        outputs.IFAD_USD_MILLION: {
                            "$sum": "${}".format(outputs.IFAD_USD_MILLION)},
                        outputs.FROM: {
                            "$min": "${}".format(columns.FROM_YEAR)},
                        outputs.TO: {"$max": "${}".format(columns.TO_YEAR)},
                        outputs.COUNTRIES: {
                            "$min": "${}".format(columns.COUNTRY)}
                    }}]
        )
        results = []
        for r in list(agg):
            f, t = nan_to_invalid_years(r[outputs.FROM], r[outputs.TO])
            results.append(
                {
                    outputs.PROJECT_IDS: r["_id"][outputs.PROJECT_IDS],
                    outputs.COSTS_SUM: round(
                        r[outputs.TOTAL_USD_MILLION], 4),
                    outputs.IFAD_COSTS: round(
                        r[outputs.IFAD_USD_MILLION], 4),
                    outputs.CURRENCY: "USD",
                    outputs.UNITS: unit,
                    outputs.FROM: int(f or None) if f else None,
                    outputs.TO: int(t or None) if t else None,
                    outputs.COUNTRIES: r[outputs.COUNTRIES]
                })
        return results

    def breakdown_by_country_from_mongo(self, unit="million"):
        if not self.results:
            return Engine.null_output()
        idx = [ObjectId(x["idx"]) for x in self.results]
        client = pymongo.MongoClient(MLAB_STRING)
        collection = client[COSTAB_DB["database"]][COSTAB_DB["collection"]]
        agg = collection.aggregate(
            [
                {"$match": {"_id": {"$in": idx}}},
                {
                    "$group": {
                        "_id": {
                            outputs.COUNTRIES: "${}".format(
                                columns.COUNTRY)},
                        outputs.PROJECT_COUNT: {
                            "$addToSet": '${}'.format(columns.PROJECT_NUMBER)},
                        outputs.TOTAL_USD_MILLION: {
                            "$sum": "${}".format(outputs.TOTAL_USD_MILLION)},
                        outputs.IFAD_USD_MILLION: {
                            "$sum": "${}".format(outputs.IFAD_USD_MILLION)},
                        outputs.FROM: {
                            "$min": "${}".format(columns.FROM_YEAR)},
                        outputs.TO: {"$max": "${}".format(columns.TO_YEAR)},
                        outputs.COUNTRIES: {
                            "$min": "${}".format(columns.COUNTRY)}
                    }}]
        )
        results = []
                    
        for r in list(agg):
            f, t = nan_to_invalid_years(r[outputs.FROM], r[outputs.TO])
            results.append(
                {
                    outputs.COUNTRIES: r["_id"][outputs.COUNTRIES],
                    outputs.COSTS_SUM: round(
                        r[outputs.TOTAL_USD_MILLION], 4),
                    outputs.IFAD_COSTS: round(
                        r[outputs.IFAD_USD_MILLION], 4),
                    outputs.CURRENCY: "USD",
                    outputs.UNITS: unit,
                    outputs.FROM: int(f or None) if f else None,

                    outputs.TO: int(t or None) if t else None,
                    # outputs.COUNTRIES: r[outputs.COUNTRIES],
                    outputs.PROJECT_COUNT: len(r[outputs.PROJECT_COUNT])
                })
        return results

    def overview_records(self, decs=2):
        if not self.results:
            return []
        ddf = self.results_df
        r = {
            outputs.PROJECT_COUNT: len(ddf[columns.PROJECT_NUMBER].unique()),
            outputs.ITEM_COUNT: len(ddf),
            outputs.COUNTRY_COUNT: len(ddf[columns.COUNTRY].unique()),
            outputs.COSTS_SUM: round(
                ddf[outputs.TOTAL_USD_MILLION].sum(), decs),
            outputs.IFAD_COSTS: round(
                ddf[outputs.IFAD_USD_MILLION].sum(), decs),
            outputs.CURRENCY: "USD",
            outputs.UNITS: "million"
        }
        return r

    def projects_records(self, decs=4):
        if not self.results:
            return []
        by_project = self.results_df.groupby(
            [columns.PROJECT_NUMBER, columns.COUNTRY])
        byp_res = []
        for (i, c), group in by_project:
            f, t = group[columns.FROM_YEAR].min(), group[columns.TO_YEAR].max()
            f, t = nan_to_invalid_years(f, t)
            byp_res.append(
                {
                    outputs.PROJECT_IDS: i,
                    outputs.COSTS_SUM: round(group[
                        outputs.TOTAL_USD_MILLION].sum(), decs),
                    outputs.IFAD_COSTS: round(group[
                        outputs.IFAD_USD_MILLION
                    ].sum(), decs),
                    outputs.COUNTRIES: c,
                    outputs.FROM: f,
                    outputs.TO: t,
                    outputs.CURRENCY: "USD",
                    outputs.UNITS: "million"
                }
            )
        return byp_res

    def countries_records(self, decs=4):
        if not self.results:
            return []
        by_country = self.results_df.groupby([columns.COUNTRY])
        byc_res = []
        for i, group in by_country:
            f, t = group[columns.FROM_YEAR].min(), group[columns.TO_YEAR].max()
            f, t = nan_to_invalid_years(f, t)
            byc_res.append(
                {
                    outputs.COUNTRIES: i,
                    outputs.COSTS_SUM: round(group[
                        outputs.TOTAL_USD_MILLION].sum(), decs),
                    outputs.IFAD_COSTS: round(group[
                        outputs.IFAD_USD_MILLION
                    ].sum(), decs),
                    outputs.PROJECT_COUNT: len(
                        group[columns.PROJECT_NUMBER].unique()),
                    outputs.FROM: f,
                    outputs.TO: t,
                    outputs.CURRENCY: "USD",
                    outputs.UNITS: "million"
                }
            )
        return byc_res

    def details_records(self, decs=4):
        if not self.results:
            return []
        f, t = columns.FROM_YEAR, columns.TO_YEAR
        ddf = self.results_df
        det_df = ddf[[
            columns.PROJECT_NUMBER,
            columns.COUNTRY,
            columns.ACTIVITY,
            columns.ITEM,
            columns.FROM_YEAR,
            columns.TO_YEAR,
            outputs.TOTAL_USD_MILLION,
            outputs.IFAD_USD_MILLION,
            columns.SHEET
        ]]
        det_df[[
            outputs.TOTAL_USD_MILLION, outputs.IFAD_USD_MILLION]] = det_df[[
                outputs.TOTAL_USD_MILLION, outputs.IFAD_USD_MILLION
            ]].round(decs)
        det_df[[f, t]] = det_df[[f, t]].astype("Int64")
        det_df.loc[det_df[f] > det_df[t], [f, t]] = None
        rename_map = {
            columns.PROJECT_NUMBER: outputs.PROJECT_IDS,
            columns.COUNTRY: outputs.COUNTRIES,
            columns.ACTIVITY: outputs.ACTIVITY,
            columns.ITEM: outputs.ITEM,
            columns.FROM_YEAR: outputs.FROM,
            columns.TO_YEAR: outputs.TO,
            columns.SHEET: outputs.SHEET,
            outputs.TOTAL_USD_MILLION: outputs.COSTS_SUM,
            outputs.IFAD_USD_MILLION: outputs.IFAD_COSTS
        }
        
        det_df = det_df.rename(columns=rename_map)
        return det_df.to_dict("records")

    def create_map(self, save=False, destination=None, zoom_start=1.5):
        world_json = json.load(open(WORLD_GEO_PATH))
        cous = {x["properties"]["adm0_a3"]: x for x in world_json["features"]}
        
        def _popup(r):
            a = (
                "Country: {}<br>"
                "Number of projects: {}<br>"
                "Total millions: {}<br>"
                "IFAD millions: {}"
            ).format(
                cous.get(r[outputs.COUNTRIES], {})
                .get("properties", {})
                .get("name", r[outputs.COUNTRIES]),
                r[outputs.PROJECT_COUNT],
                r[outputs.COSTS_SUM],
                r[outputs.IFAD_COSTS]
            )
            return a
        
        if save and not destination:
            destination = "map.html"
        byc_res = self.breakdown_by_country_from_mongo()
        choro = folium.Choropleth(
            geo_data=WORLD_GEO_PATH,
            name='choropleth',
            data=pd.DataFrame(byc_res),
            columns=[outputs.COUNTRIES, outputs.IFAD_COSTS],
            key_on='feature.properties.adm0_a3',
            fill_color='YlGn',
            fill_opacity=0.7,
            line_opacity=0.2,
            legend_name='Costs IFAD (USD millions)',
            nan_fill_opacity=1,
            nan_fill_color="white"
        )
        centers = []
        gr = folium.FeatureGroup(name='nations')
        print(sorted([x["Country"] for x in byc_res]))
        for x in byc_res:
            geom = cous.get(x["Country"], {}).get("geometry")
            try:
                center = shape(geom).centroid
            except:
                print(x["Country"])
                continue
            centers.append(center)
            lon, lat = center.x, center.y
            folium.Marker(
                location=[lat, lon],
                # icon=folium.Icon("glyphicon-pushpin"),
                popup=folium.Popup(_popup(x), max_width=700)
            ).add_to(gr)
        # multip = MultiPoint(centers).centroid
        # x0, y0 = multip.x, multip.y
        m = folium.Map(
            zoom_start=zoom_start,
            # location=[y0, x0],
            tiles='https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png',
            attr="<a href=https://endless-sky.github.io</a>",
            min_zoom=zoom_start
        )

        gr.add_to(m)
        choro.add_to(m)
        if save:
            m.save(destination)
        return m


@click.command()
@click.argument(
    "query",
    metavar="QUERY",
    type=click.STRING
)
@click.option(
    "--index_dir", "-i",
    metavar="OUTPUT",
    type=click.Path(),
    default="db_index_mongo")
def main(query, index_dir):
    print(WORLD_GEO_PATH)
    print("querying whoosh")
    engine = Engine(index_dir)
    engine.search(query, engine_parser=MONGO_PARSER)
    # print("build_df")
    # df = engine.search_data()
    # print("breakdown")
    # out = engine.breakdown_dollarcosts(df)
    # out.update(engine.general_info(df))
    print(WORLD_GEO_PATH)
    print("querying mongodb")
    out = engine.breakdown_dollarcosts_from_mongo()
    out.update(engine.general_info_from_mongo())
    engine.create_map(save=True, destination="../data/map.html")
    print(len(engine.breakdown_by_project_from_mongo()))


if __name__ == "__main__":
    main()
