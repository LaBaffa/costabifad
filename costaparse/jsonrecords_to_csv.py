import json
import pandas as pd
from costaparse.settings.config import (
    SHEET_BLACK_LIST, COST_KEYS, MULTI_INDEX
)
from costaparse.settings.logging_settings import extractlog
import os
import click


this_path = os.path.dirname(os.path.realpath(__file__))
PWD = this_path
RECORDS_PATH = os.path.join(PWD, "out.json")
CSV_PATH = os.path.join(PWD, "extracted_items.csv")


@click.command()
@click.argument(
    "input_file",
    metavar="INPUT",
    type=click.Path(exists=True)
)
@click.option(
    "--out", "-o",
    metavar="OUTPUT",
    type=click.Path(),
    default=CSV_PATH)
@click.option("--excel", "ext", flag_value="excel")
@click.option("--csv", "ext", flag_value="csv", default=True)
def main(input_file, out, ext):
    extractlog.info("reading records from {}".format(input_file))
    records = json.load(open(input_file))
    extractlog.info("creating dataframe")
    fmt_data = [
        {
            k: v if isinstance(v, dict) else {"value": v}
            for k, v in d.items()
        }
        for d in records
    ]
    outs = {
        idx: [
            d[idx[0]].get(idx[1] if idx[1] else "value")
            for d in fmt_data
        ]
        for idx in MULTI_INDEX
    }
    df = pd.DataFrame(outs)
    df = df[~df["sheet"].isin([x[0] for x in SHEET_BLACK_LIST])]
    df = df[~df[COST_KEYS].isnull().all(axis=1)]
    multi_cols = pd.MultiIndex.from_tuples(MULTI_INDEX)
    df = pd.DataFrame(df, columns=multi_cols)
    if ext == "csv":
        out = os.path.splitext(out)[0] + ".csv"
        extractlog.info("saving dataframe as csv on {}".format(out))
        df.to_csv(out, index=False)
    else:
        out = os.path.splitext(out)[0] + ".xlsx"
        extractlog.info("saving dataframe as excel on {}".format(out))
        writer = pd.ExcelWriter(out, engine='xlsxwriter')
        df.to_excel(writer)
        writer.save()
        writer.close()


if __name__ == "__main__":
    main()
