import pandas as pd
import numpy as np
from costaparse.settings.dispatchers import (
    RECORD_DISPATCHER, ITEM_DISPATCHER
)
import costaparse.util.column_finders as cf
from costaparse.settings.logging_settings import extractlog
import re
from costaparse.settings.config import (
    SUBTOTAL_PREFIXES, FIRST_ROW_REFERENCES, INVESTMENT_SHEET_PREFIXES,
    INVESTMENTCOSTS_KEYWORDS
)


def load(costab_file):
    return pd.ExcelFile(costab_file)


def cost_sheet_names(xls_obj):
    names = [
        s
        for s in xls_obj.sheet_names
        for prefix in INVESTMENT_SHEET_PREFIXES
        if s.startswith(prefix)]
    return names


def df_from_name(name, xls_obj):
    df = xls_obj.parse(name, header=None)
    df.columns = range(df.shape[1])
    return df


def first_valid_row(df):
    first_row = None
    query = "|".join(FIRST_ROW_REFERENCES)
    for col in df.columns:
        series = df[col].astype(str)
        mask = series.str.lower().str.contains(
            query
        )
        if mask.any():
            first_row = np.where(mask)[0][0]
            break
    return first_row


def investmentcosts_rows(df):
    start = None
    end = None
    for i, r in df.iterrows():
        mask = r.notnull()
        if not mask.any():
            continue
        idx = np.where(mask)[0][0]
        first = r.astype(str)[idx]

        if start is None:
            if first.strip() in INVESTMENTCOSTS_KEYWORDS:
                start = (i, idx)
        else:
            if idx <= start[1]:
                end = (i - 1, idx)
                break
    return start, end


def recurrentcosts_rows(df):
    out = None, None
    recurrent_pos = cf.recurrentcosts_pos(df)
    if recurrent_pos[0] is not None:
        out = recurrent_pos, (len(df), recurrent_pos[1])
    return out


def investmentcosts_rows(df):
    investmentcosts_pos = cf.investmentcosts_pos(df)
    recurrent_pos = cf.recurrentcosts_pos(df)
    out = None, None
    if investmentcosts_pos == (None, None):
        return (None, None)
    if recurrent_pos == (None, None):
        a, b = investmentcosts_pos
        out = (a, b), (len(df), b)
    else:
        out = (
            investmentcosts_pos,
            (recurrent_pos[0] - 1, investmentcosts_pos[1])
            )
    return out


def get_records(df, rows, ref_labels):
    a, b = rows
    if a is None:
        return []
    start_costs, end_costs = a[0], b[0]
    investment_costs = df.loc[start_costs + 1: end_costs]
    extractlog.debug(ref_labels)
    # columns_to_get = [
    #     v(ref_cols) for k, v in COLUMNS_DISPATCHER.items()
    # ]
    items_to_get = [
        v(ref_labels) for k, v in ITEM_DISPATCHER.items()
    ]
    levels = {}
    items = []
    extractlog.debug((a, b))
    for i, r in investment_costs.iterrows():
        mask = r.notnull()
        if not mask.any():
            continue
        idx = np.where(mask)[0][0]
        first = r.astype(str)[idx]
        has_numbers = pd.to_numeric(r, errors="coerce").any()
        # is_feature = first.strip().lower() not in (
        #     "total", "subtotal", "sub total"
        # )
        gex = "^\\s*{}\\b".format("|".join(SUBTOTAL_PREFIXES))
        is_feature = len(re.findall(gex, first.strip().lower())) == 0
        if not has_numbers and is_feature:
            levels[idx] = first.strip()
        elif has_numbers and is_feature:
            levels[idx] = first.strip()
            fields = [
                levels[level] for level in sorted(levels)
                if level <= idx]
            items.append(
                [
                    fields, [
                        {
                            **i,
                            "value": r[i["value_col"]]
                            if i.get("value_col") is not None else None}
                        for i in items_to_get
                    
                    ]])
    return items


def jsonify_records(records, **kw):
    out = []
    for r in records:
        fields = {k: v(r[0]) for k, v in RECORD_DISPATCHER.items()}
        values = dict(zip(ITEM_DISPATCHER, r[1]))
        fields.update(values)
        fields.update(kw)
        out.append(fields)
    return out

