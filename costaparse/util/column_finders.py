from costaparse.settings.config import (
    COSTS_HIERARCHY, COSTS_LABEL, UNIT_LABEL, REFERENCE_LABELS,
    SUBCOMPONENT_PREFIX, YEARS_RANGE, UNIT_STRINGS, COL_REGEX
    )
from costaparse.settings import columns, regexes
from costaparse.settings.logging_settings import extractlog
from collections import defaultdict
import copy
import numpy as np
import re
import pandas as pd


DOLLARS = ["usd", "$", "us$", "usd$"]


def find_substrings_in_columns(reference_columns):
    doubles = {}
    for label, cols in reference_columns.items():
        subs = [
            x for x in reference_columns
            if x != label and label in x
        ]
        if subs:
            doubles[label] = subs
    return doubles


def filter_doubles_in_columns(reference_columns):
    columns = copy.deepcopy(reference_columns)
    doubles = find_substrings_in_columns(reference_columns)
    for child, parent in doubles.items():
        parent_cols = [
            c for p in parent for c in columns[p]
        ]
        new_cols = [
            c for c in columns[child]
            if c not in parent_cols
        ]
        columns[child] = new_cols
    columns = {k: v for k, v in columns.items() if v}
    return columns


def reference_columns(df_afterfirstvalid):
    labels_info = defaultdict(list)
    for col in df_afterfirstvalid.columns:
        for label in REFERENCE_LABELS:
            series = df_afterfirstvalid[col]
            mask = (
                    series.astype(str).
                    str.lower().
                    str.split("(").
                    str[0].
                    str.strip() == label
            )
            if mask.any():
                labels_info[label].append(
                    (series[mask].index[0], series.name)
                )
    return labels_info


def reference_columns_re(df):
    labels_info = defaultdict(list)
    for col in df.columns:
        for label in regexes.COLUMNS:
            series = df[col]
            matches = (
                series.astype(str).
                str.lower().
                str.strip().
                str.extract(regexes.COLUMNS.get(label))
            )
            valid_matches = matches.dropna(subset=["label"])
            if not valid_matches.empty:
                mask = matches["label"].notnull()
                labels_info[label].append(
                    {
                        "index": int(series[mask].index[0]),
                        "column": int(series.name),
                        **valid_matches.to_dict("records")[0]
                    }
                )
    return labels_info


def reference_columns_join(df):
    labels_info = defaultdict(list)
    for col in df.columns:
        for label, gex in regexes.COLUMNS.items():
            sens_triggered = df[col].astype(str).str.contains(
                regexes.REGEX_TRIGGER_SENS[label])
            if not sens_triggered.any():
                continue
            if label == columns.IFAD:
                # print(gex)
                pass
            if label in COL_REGEX:
                series = pd.Series(
                    " ".join(df[col].dropna().astype(str).values.tolist()))
            else:
                series = df[col]
            matches = (
                series.astype(str).
                str.lower().
                str.strip().
                str.extract(gex)
            )
            valid_matches = matches.dropna(subset=["label"])
            if not valid_matches.empty:
                mask = matches["label"].notnull()
                labels_info[label].append(
                    {
                        "index": int(series[mask].index[0]),
                        "column": int(col),
                        **valid_matches.to_dict("records")[0]
                    }
                )
    return labels_info

    
def total_after_column(ref_labels, col, total_labels=COSTS_LABEL):
    if col.get("column") is not None:
        total_cols = sorted([
            x.get("column")
            for c in COSTS_LABEL
            for x in ref_labels.get(c, [])
        ])
        return next((x for x in total_cols if x >= col.get("column")), None)
    return None


def cost_type_item(ref_labels, label):
    items = ref_labels.get(label, [{}])
    item = next(
        (
            x
            for x in items
            if x.get("currency") in DOLLARS
        ), items[0])
    return item


def unit_column(ref_labels):
    return ref_labels.get(UNIT_LABEL, [{}])[0].get("column")
    

def costs_column(ref_labels):
    for h in COSTS_HIERARCHY:
        item = cost_type_item(ref_labels, h)
        total_col = total_after_column(ref_labels, item)
        if item.get("column") is not None and total_col is None:
            extractlog.error(
                "costs label seems to have unknown pattern")
            return None
        elif total_col is not None:
            return total_col


def contingencies_column(ref_labels):
    item = cost_type_item(ref_labels, columns.CONTINGENCIES)
    return total_after_column(
        ref_labels, item)
    

def base_cost_column(ref_labels):
    item = cost_type_item(ref_labels, columns.BASE_COST)
    return total_after_column(
        ref_labels, item)


def quantities_column(ref_labels):
    item = cost_type_item(ref_labels, columns.QUANTITIES)
    return total_after_column(
        ref_labels, item
    )
    

def contingencies_item(ref_labels):
    item = cost_type_item(ref_labels, columns.CONTINGENCIES)
    tot_col = total_after_column(ref_labels, item)
    return {**item, "value_col": int(tot_col) if tot_col is not None else None}


def base_cost_item(ref_labels):
    item = cost_type_item(ref_labels, columns.BASE_COST)
    tot_col = total_after_column(ref_labels, item)
    return {**item, "value_col": int(tot_col) if tot_col is not None else None}


def quantities_item(ref_labels):
    item = cost_type_item(ref_labels, columns.QUANTITIES)
    tot_col = total_after_column(ref_labels, item)
    return {**item, "value_col": int(tot_col) if tot_col is not None else None}


def unit_item(ref_labels):
    item = ref_labels.get(columns.UNIT, [{}])[0]
    return {**item, "value_col": item.get("column")}


def finrule_item(ref_labels):
    item = ref_labels.get(columns.FIN_RULE, [{}])[0]
    return {**item, "value_col": item.get("column")}


def exp_account_item(ref_labels):
    item = ref_labels.get(columns.EXP_ACCOUNT, [{}])[0]
    return {**item, "value_col": item.get("column")}


def disb_account_item(ref_labels):
    item = ref_labels.get(columns.DISB_ACCOUNT, [{}])[0]
    return {**item, "value_col": item.get("column")}


def financier_costs(ref_labels, financier):
    pos = None
    exp_fin = cost_type_item(ref_labels, columns.BREAKDOWN)
    if exp_fin.get("column") is not None:
        fin_cols = [x["column"] for x in ref_labels.get(financier, [])]
        totals = [x["column"] for x in ref_labels.get(columns.TOTAL, [])]
        if fin_cols and totals:
            col = min(fin_cols, key=lambda x: abs(exp_fin.get("column") - x))
            pos = next((x for x in totals if x > col), None)
    return pos


def ifad_costs_column(ref_labels):
    return financier_costs(ref_labels, columns.IFAD)


def gef_costs_column(ref_labels):
    return financier_costs(ref_labels, columns.GEF)


def asap_costs_column(ref_labels):
    return financier_costs(ref_labels, columns.ASAP)

   
def finrule_column(ref_labels):
    return ref_labels.get(columns.FIN_RULE, [{}])[0].get("column")


def breakdown_item(ref_labels, financier):
    pos = None
    fin_item = ref_labels.get(financier, [None])[0]
    if fin_item is None:
        return {
            "value_col": None, "label": financier,
            "currency": None, "unit": None
        }
    exp_fin = cost_type_item(ref_labels, columns.BREAKDOWN)
    totals = [x.get("column") for x in ref_labels.get(columns.TOTAL, [])]
    if exp_fin.get("column") is not None:
        f_cols = [x["column"] for x in ref_labels.get(financier, [])]
        if f_cols and totals:
            col = min(f_cols, key=lambda x: abs(exp_fin.get("column") - x))
            pos = next((int(x) for x in totals if x > col), None)
    else:
        pos = total_after_column(ref_labels, fin_item)
    return {
        "value_col": pos,
        "label": financier,
        "currency": exp_fin.get("currency"),
        "unit": exp_fin.get("unit")
    }


def ifad_costs_item(ref_labels):
    return breakdown_item(ref_labels, columns.IFAD)


def gef_costs_item(ref_labels):
    return breakdown_item(ref_labels, columns.GEF)


def asap_costs_item(ref_labels):
    return breakdown_item(ref_labels, columns.ASAP)


def ifad_loan_item(ref_labels):
    return breakdown_item(ref_labels, columns.IFAD_LOAN)


def ifad_grant_item(ref_labels):
    return breakdown_item(ref_labels, columns.IFAD_GRANT)


def subcomponent(df):
    subcomp = None
    for i, r in df.iterrows():
        mask = r.astype(str).str.lower().str.startswith(SUBCOMPONENT_PREFIX)
        if mask.any():
            idx = np.where(mask)[0][0]
            subcomp = r[idx].lower().lstrip(" " + SUBCOMPONENT_PREFIX)
            break
    return subcomp


def activity_in_record(record):
    if len(record) == 0:
        out = None
    else:
        out = record[0]
    return out


def item_in_record(record):
    if len(record) <= 1:
        out = None
    else:
        out = " ".join(record[1:])
    return out


def component(pdr, costab_file):
    return None


def component_number(sheet_name):
    return ".".join(sheet_name.split("_")[1:])


def set_years_range():
    return YEARS_RANGE


# def proj_number(fname):
#     years_range = set_years_range()
#     gex = "\\d{4}"
#     candidates = re.findall(gex, fname)
#     project = next(
#         (
#             x for x in candidates
#             if int(x) not in years_range
#         ),
#         None)
#     return project

def proj_number(fname):
    # base, tail = os.path.split(fname)
    tail = fname
    try:
        n = re.findall(regexes.PROJECT_NUMBER, tail)[0]
    except IndexError:
        n = None
    return n


def country_candidates(df):
    f = df.iloc[0].dropna()
    return f[
        f.astype(str).str.strip().astype(bool)
    ].astype(str).to_list()


def base_cost_pos(df):
    out = None, None
    for col in df.columns:
        series = df[col]
        mask = series.astype(
                str).str.lower().str.contains(
            "base cost"
        )
        if mask.any():
            out = series[mask].index[0], series.name
            break
    return out


def detailed_costs_pos(df):
    out = None, None
    for col in df.columns:
        series = df[col]
        mask = series.astype(str).str.lower().str.contains(
            "detailed .*?costs"
        )
        if mask.any():
            out = series[mask].index[0], series.name
            break
    
    return out


def contingencies_costs_pos(df):
    out = None, None
    for col in df.columns:
        series = df[col]
        mask = series.astype(str).str.lower().str.contains(
            "totals including contingencies"
        )
        if mask.any():
            out = series[mask].index[0], series.name
            break
    return out


def recurrentcosts_pos(df):
    out = None, None
    # query = "|".join([x.lower() for x in RECURRENT_COSTS_KEYWORDS])
    for col in df.columns:
        series = df[col]
        mask = series.astype(str).str.lower().str.strip().str.contains(
            regexes.RECURRENTCOSTS
        )
        if mask.any():
            out = series[mask].index[0], series.name
            break
    return out


def investmentcosts_pos(df):
    out = None, None
    # query = "|".join([x.lower() for x in INVESTMENTCOSTS_KEYWORDS])
    for col in df.columns:
        series = df[col]
        mask = series.astype(str).str.lower().str.strip().str.contains(
            regexes.INVESTMENTCOSTS
        )
        if mask.any():
            out = series[mask].index[0], series.name
            break
    return out


def currency_candidates(df, candidates_pos):
    candidates_pos = [
        (i, col)
        for i, col in candidates_pos
        if i is not None and col is not None and i+1 < len(df)
    ]
    
    def cols(c): return [x for x in range(c - 1, c + 2) if x >= 0]
    
    def rows(r): return [r, r + 1]
    out = [
        df[c][r]
        for i, col in candidates_pos
        for c in cols(col)
        for r in rows(i)
        if isinstance(df[c][r], str)
    ]
    return out


def currency_and_unit(currency_candidates):
    out = []
    for c in currency_candidates:
        open_bra = c.find("(")
        if open_bra >= 0:
            inside = c[open_bra+1:c.find(")")].lower()
            out.extend(inside.split())
    currency_unit = next((
        UNIT_STRINGS.get(x.lower()) for x in out
        if UNIT_STRINGS.get(x.lower()) is not None),
        1)

    out = [x for x in out if x not in UNIT_STRINGS]
    currency = next((x.lower() for x in out), "us$")
    return currency, currency_unit


def currency_and_unit_re(candidates):
    currency, unit = None, None
    series = pd.Series(" ".join(candidates))
    gex = regexes.CURRENCY_AND_UNIT
    matches = (
        series.astype(str).
        str.lower().
        str.strip().
        str.extract(gex)
    )
    valid_matches = matches.dropna(subset=["label"])
    if not valid_matches.empty:
        d = valid_matches.iloc[0]
        currency, unit = d["currency"], d["unit"]
    return currency, unit
