from costaparse.settings import columns
from costaparse.settings.config import UNIT_STRINGS

SPECIAL_REGEX_CHARS = ". \\ + * ? [ ^ ] $ ( ) { } = ! < > | : -".split()


def acronyms(s, seps=("", " ", ".")):
    return ["".join((sep.join(s), sep)).strip() for sep in seps]


def acro_regex(s, seps=("", " ", ".")):
    seps = [
        (x + "*", "\\" + x + "*")[x in SPECIAL_REGEX_CHARS]
        if x else x
        for x in seps
    ]
    return "|".join(acronyms(s, seps))


# PROJECT_NUMBER = "(?<=\\d{4})(\\d{4})(?=\\.|_)"  # this to take last 4 digits
C_UNITS = "|".join(UNIT_STRINGS.keys())

INVESTMENTCOSTS = "^i\\.\\s*investment costs|^investment costs"
RECURRENTCOSTS = "^ii\\.\\s*recurrent costs|^recurrent costs"
PROJECT_NUMBER = "\\d{9,10}"
WORDS_IN_CAMEL_CASE_FILE = "\\b(\\w+?)(?=_|\\.|-|\\b)"
CURRENCY_AND_UNIT = (
    "(?P<label>\\("
    "(?P<currency>[a-z\\s\\$]*?)?\\s*?"
    "(?P<unit>{cu})?\\))").format(cu=C_UNITS)


COLUMNS = {
    columns.CONTINGENCIES: (
        "(?P<label>{c})"
        "(?:\\s*\\((?P<currency>[a-z\\s\\$]*?)?"
        "\\s*?(?P<unit>{cu})?\\))?").format(
            c=columns.CONTINGENCIES,
            cu=C_UNITS
    ),
    columns.BASE_COST:  (
        "^(?P<label>{c})"
        "(?:\\s*\\((?P<currency>[a-z\\s\\$]*?)?"
        "\\s*?(?P<unit>{cu})?\\))?").format(
            c=columns.BASE_COST,
            cu=C_UNITS
        ),
    columns.UNIT: "^(?P<label>{})$".format(columns.UNIT),
    # columns.UNIT_COST:  (
    #     "^(?P<label>{c})"
    #     "(?:\\s*\\((?P<currency>[a-z\\s\\$]*?)?\\s*?(?P<unit>{cu})?\\))?").format(
    #         c=columns.UNIT_COST,
    #         cu=C_UNITS)
    # ),
    columns.IFAD: (
        "^(?P<label>(?:{ifad}|{fida})"
        "(?:\\s+"
        "(?!loan\\b|grant\\b|asap\\b|gef\\b|.*%)\\w+|$)"
        ")").format(
            ifad=acro_regex("ifad"), fida=acro_regex("fida")),
    columns.IFAD_LOAN: "^(?P<label>(?:{ifad}|{fida})\\s*loan)$".format(
        ifad=acro_regex("ifad"), fida=acro_regex("fida")
    ),
    columns.IFAD_GRANT: "^(?P<label>(?:{ifad}|{fida})\\s*grant)$".format(
        ifad=acro_regex("ifad"), fida=acro_regex("fida")
    ),
    columns.BREAKDOWN: (
        "(?P<label>{c})"
        "(?:\\s*\\((?P<currency>[a-z\\s\\$]*?)?"
        "\\s*?(?P<unit>{cu})?\\))?").format(
            c=columns.BREAKDOWN,
            cu=C_UNITS
        ),
    columns.FIN_RULE: "^(?P<label>fin[a-z\\.]*?\\s+rule|financing)$",
    columns.QUANTITIES: "^(?P<label>{})$".format(columns.QUANTITIES),
    columns.TOTAL: "^(?P<label>total|total\\s+\\$)(?:\\s*\\(.*?\\))?$",
    columns.GEF: "\\b(?P<label>(?:{}))(?:\\b|\\s|$)".format(
        acro_regex(columns.GEF)
    ),
    columns.ASAP: "\\b(?P<label>(?:{}))(?:\\b|\\s|$)".format(
        acro_regex(columns.ASAP)
    ),
    columns.EXP_ACCOUNT: "^(?P<label>expenditure|expenditure\\s+account)$",
    columns.DISB_ACCOUNT: "^(?P<label>disb\\.\\s*(?:account|acct\\.))$"
}

YEAR_AFTER_1000 = "\\b[12][0-9]{3}\\b"
YEAR_TWO_DIGITS = "\\b[0-9]{2}\\b"
YEAR_FOUR_TWO = "({by}|{sy})(?:\\/({by}|{sy}))?".format(
    by=YEAR_AFTER_1000, sy=YEAR_TWO_DIGITS)


REGEX_TRIGGER_SENS = {
    columns.CONTINGENCIES: "Contingencies",
    columns.BASE_COST: "Base",
    columns.UNIT: "Unit",
    columns.IFAD: "{}|{}".format(acro_regex("IFAD"), acro_regex("FIDA")),
    columns.IFAD_LOAN: "{}|{}".format(acro_regex("IFAD"), acro_regex("FIDA")),
    columns.IFAD_GRANT: "{}|{}".format(acro_regex("IFAD"), acro_regex("FIDA")),
    columns.BREAKDOWN: "Expenditures",
    columns.FIN_RULE: "Rule|rule",
    columns.QUANTITIES: "Quantities",
    columns.TOTAL: "Total",
    columns.GEF: acro_regex("GEF"),
    columns.ASAP: acro_regex("ASAP"),
    columns.EXP_ACCOUNT: "Expenditure",
    columns.DISB_ACCOUNT: "Disb"
}
