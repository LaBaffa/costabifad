from costaparse.settings import columns

INVESTMENT_SHEET_PREFIXES = ["DT", "Cat", "comp"]
COSTAB_FILE_TEMPLATE = "{nation_code}_{project_number}_{year}_{plus}.{ext}"
FIRST_ROW_REFERENCE_CELL = "base cost"
FIRST_ROW_REFERENCES = ["base cost", "unit cost", "including contingencies"]
INVESTMENTCOSTS_KEYWORDS = ["I. Investment Costs", "Investment Costs"]
RECURRENT_COSTS_KEYWORDS = ["II. Recurrent Costs", "Recurrent Costs"]
REFERENCE_LABELS = [
    columns.CONTINGENCIES,
    columns.BASE_COST,
    columns.UNIT,
    columns.UNIT_COST,
    columns.TOTAL,
    columns.QUANTITIES,
    # columns.TOTAL_DOLLAR,
    columns.IFAD,
    columns.BREAKDOWN,
    columns.FIN_RULE
]
COSTS_HIERARCHY = [
    columns.CONTINGENCIES,
    columns.BASE_COST,
    columns.IFAD
    # "total $"
]

COSTS_LABEL = [columns.TOTAL,  columns.TOTAL_DOLLAR]
UNIT_LABEL = columns.UNIT
QUANTITIES_LABEL = columns.QUANTITIES
SUBCOMPONENT_PREFIX = "table"

OUTPUT_COLUMNS = [
    "unit",
    "quantities",
    "currency",
    "component",
    "sub-component",
    "activity",
    "item",
    "costs",
    "currency_unit",
    "component_number",
    "project_number"
]
DESCRIPTIVE_COLUMNS = [
    "component",
    "subcomponent",
    "activity",
    "item"
]
VALUE_COLUMNS = [
    "unit",
    "quantities",
    "currency",
    "costs"
]
YEARS_RANGE = range(2013, 2018)

UNIT_STRINGS = {
    "million": 1e6,
    "millions": 1e6,
    "'000": 1e3,
    "thousands": 1e3,
    "thousand": 1e3,
    "1": 1
}


SUBTOTAL_PREFIXES = ["total", "subtotal", "sub total"]
CSV_COLUMNS = [
    "file_name",
    "country",
    "project_number",
    # "year",
    "component_number",
    "component",
    "subcomponent",
    "activity",
    "item",
    "unit",
    "quantities",
    "currency",
    "currency_unit",
    "costs",
    "num_base_cost",
    "num_contingencies",
    "num_unit_cost"
]


MULTI_INDEX = [
    ("file_name", ""),
    ("sheet", ""),
    ("country", ""),
    ("project_number", ""),
    ("from_year", ""),
    ("to_year", ""),
    ("subcomponent", ""),
    ("activity", ""),
    ("item", ""),
    ("unit", ""),
    ("quantities", ""),
    ("cost_type", ""),
    ("base cost", "value"),
    ("base cost", "currency"),
    ("base cost", "unit"),
    ("totals including contingencies", "value"),
    ("totals including contingencies", "currency"),
    ("totals including contingencies", "unit"),
    ("ifad", "value"),
    ("ifad", "currency"),
    ("ifad", "unit"),
    ("ifad_loan", "value"),
    ("ifad_loan", "currency"),
    ("ifad_loan", "unit"),
    ("ifad_grant", "value"),
    ("ifad_grant", "currency"),
    ("ifad_grant", "unit"),
    ("gef", "value"),
    ("gef", "currency"),
    ("gef", "unit"),
    ("asap", "value"),
    ("asap", "currency"),
    ("asap", "unit"),
    ("expenditure_account", ""),
    ("disb_account", ""),
    ("fin. rule", ""),
    ("num_base_cost", ""),
    ("num_contingencies", ""),
    ("sheet_currency", ""),
    ("sheet_currency_unit", "")
]

SHEET_BLACK_LIST = [
    ['EXPFIN', 'Laos_1100001608_COSTAB.xls'],
    ['SUMEA', 'Laos_1100001608_COSTAB.xls'],
    ['EXCOMF', 'Laos_1100001608_COSTAB.xls'],
    ['EXCMTF', 'Laos_1100001608_COSTAB.xls'],
    ['EAYRB', 'Laos_1100001608_COSTAB.xls'],
    ['EAYRT', 'Laos_1100001608_COSTAB.xls'],
    ['EABRKF', 'Laos_1100001608_COSTAB.xls'],
    ['FINBYR', 'togo_1100001639_costab.xls'],
    ['EXCMTL', 'togo_1100001639_costab.xls'],
    ['PHYAGG', 'Laos_1100001459_COSTAB.xls'],
    ['14.6 Annex 2 Financing Tables', 'Laos_1100001207_COSTAB.xls'],
    ['EXCOML', 'China_1100001478_COSTAB.xls'],
    ['EXPFIN-USD', 'Nepal_1100001119_COSTAB.xls'],
    ['EXPFIN-NPR', 'Nepal_1100001119_COSTAB.xls'],
    ['EABRKL', 'India_1100001348_COSTAB.xls'],
    ['EXPFIN (2)', 'China_1100001454_COSTAB.xls'],
    ['SUMFIN', 'Bhutan_1100001482_COSTAB.xls'],
    ['SUMEA (2)', 'China_1100001223_COSTAB.xlsx'],
    ['EXCMTF (2)', 'China_1100001223_COSTAB.xlsx'],
    ['PHYAGG (2)', 'Vietnam_1100001422_COSTAB.xls'],
    ['PHYCOMF', 'Vietnam_1100001422_COSTAB.xls'],
    ['EXCOMF (2)', 'Vietnam_1100001422_COSTAB.xls'],
    ['EAYRT (2)', 'Vietnam_1100001422_COSTAB.xls'],
    ['Table 1', 'Vietnam_1100001422_COSTAB.xls'],
    ['Table 2', 'Vietnam_1100001422_COSTAB.xls'],
    ['Table 3', 'Vietnam_1100001422_COSTAB.xls'],
    ['2SUMEA', 'Ethiopia_1100001521_COSTAB.xlsx'],
    ['4EXCOML', 'Ethiopia_1100001521_COSTAB.xlsx'],
    ['5EXCOMF', 'Ethiopia_1100001521_COSTAB.xlsx'],
    ['6EXCMTL', 'Ethiopia_1100001521_COSTAB.xlsx'],
    ['7EXCMTF', 'Ethiopia_1100001521_COSTAB.xlsx'],
    ['10EAYRT', 'Ethiopia_1100001521_COSTAB.xlsx'],
    ['11EABRKL', 'Ethiopia_1100001521_COSTAB.xlsx'],
    ['12EABRKF', 'Ethiopia_1100001521_COSTAB.xlsx'],
    # ['Annex 2MoneraDetailCost', 'sri lanka_1100001316_COSTAB.xls'],
    # ['Detail Cost table for Moneragal', 'sri lanka_1100001316_COSTAB.xls'],
    # ['All Cost tables includ Summary', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 1', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 2', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 3', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 4', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 5', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 6', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 7', 'sri lanka_1100001316_COSTAB.xls'],
    # ['COM 8', 'sri lanka_1100001316_COSTAB.xls'],
    ['expfin1', 'Philippines_1100001485_COSTAB.xls'],
    ['finbyr1', 'Philippines_1100001485_COSTAB.xls'],
    ['excomf1', 'Philippines_1100001485_COSTAB.xls'],
    ['excmtf1', 'Philippines_1100001485_COSTAB.xls'],
    ['eayrb1', 'Philippines_1100001485_COSTAB.xls'],
    ['earyt1', 'Philippines_1100001485_COSTAB.xls'],
    ['eabrkf1', 'Philippines_1100001485_COSTAB.xls'],
    ['EXPFIN_decimal', 'Bangladesh_1100001585_COSTAB.xlsx'],
    ['SUMEA_decimal', 'Bangladesh_1100001585_COSTAB.xlsx'],
    ['EXCOML_decimal', 'Bangladesh_1100001585_COSTAB.xlsx'],
    ['EXCOMF_decimal', 'Bangladesh_1100001585_COSTAB.xlsx'],
    ['EXCMTF_decimal', 'Bangladesh_1100001585_COSTAB.xlsx'],
    ['EAYRB_decimal', 'Bangladesh_1100001585_COSTAB.xlsx'],
    ['EAYRT_decimal', 'Bangladesh_1100001585_COSTAB.xlsx'],
    ['ESMA', 'Pakistan_1100001245_COSTAB.XLS'],
    ['EAFIN', 'Pakistan_1100001245_COSTAB.XLS'],
    ["COMFIN", "Comoros_2000001157_COSTAB.xlsx"],
    ["EXPFIN", "Comoros_2000001157_COSTAB.xlsx"],
    ["LFTFIN", "Comoros_2000001157_COSTAB.xlsx"],
    ["SEMFIN", "Comoros_2000001157_COSTAB.xlsx"],
    ["ALLOCL", "Comoros_2000001157_COSTAB.xlsx"],
    ["ALLOCF", "Comoros_2000001157_COSTAB.xlsx"],
    ["FINFL", "Comoros_2000001157_COSTAB.xlsx"],
    ["SUMCOM", "Comoros_2000001157_COSTAB.xlsx"],
    ["SUMEA", "Comoros_2000001157_COSTAB.xlsx"],
    ["EXCMTL", "Comoros_2000001157_COSTAB.xlsx"],
    ["EXCMTF", "Comoros_2000001157_COSTAB.xlsx"],
    ["COMYRT", "Comoros_2000001157_COSTAB.xlsx"],
    ["COMYIR", "Comoros_2000001157_COSTAB.xlsx"],
    ["EAYRT", "Comoros_2000001157_COSTAB.xlsx"],
    ["EABRKL", "Comoros_2000001157_COSTAB.xlsx"],
    ["EABRKF", "Comoros_2000001157_COSTAB.xlsx"],
    ["FINBYR", "Comoros_2000001157_COSTAB.xlsx"],
    ["PROYRS", "sierra leone_1100001054_costab.xlsx"],
    ["DISFIN", "sierra leone_1100001054_costab.xlsx"]

]


COST_KEYS = [
    "base cost",
    "totals including contingencies",
    "ifad",
    "ifad_loan",
    "ifad_grant",
    "gef",
    "asap"
]

CELL_REGEX = [
    columns.UNIT,
    columns.IFAD,
    columns.IFAD_LOAN,
    columns.IFAD_GRANT,
    columns.FIN_RULE,
    columns.QUANTITIES,
    columns.TOTAL,
    columns.GEF,
    columns.ASAP,
    columns.EXP_ACCOUNT,
    columns.DISB_ACCOUNT
]

COL_REGEX = [
    columns.CONTINGENCIES,
    columns.BASE_COST,
    columns.BREAKDOWN
]

