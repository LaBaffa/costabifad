from costaparse.util.column_finders import (
    costs_column, unit_column, activity_in_record, item_in_record,
    quantities_column, base_cost_column, ifad_costs_column,
    finrule_column, contingencies_column, gef_costs_column,
    asap_costs_column
    )
import costaparse.util.column_finders as cf
from collections import OrderedDict
from costaparse.settings.config import QUANTITIES_LABEL, UNIT_LABEL
from costaparse.settings import columns


COLUMNS_DISPATCHER = OrderedDict([
    (UNIT_LABEL, unit_column),
    ("costs", costs_column),
    (QUANTITIES_LABEL, quantities_column),
    (columns.BASE_COST, base_cost_column),
    (columns.CONTINGENCIES, contingencies_column),
    (columns.IFAD_COSTS, ifad_costs_column),
    (columns.FIN_RULE, finrule_column),
    (columns.GEF, gef_costs_column),
    (columns.ASAP, asap_costs_column)
]
)

ITEM_DISPATCHER = OrderedDict([
    (columns.UNIT, cf.unit_item),
    (columns.QUANTITIES, cf.quantities_item),
    (columns.BASE_COST, cf.base_cost_item),
    (columns.CONTINGENCIES, cf.contingencies_item),
    (columns.FIN_RULE, cf.finrule_item),
    (columns.IFAD, cf.ifad_costs_item),
    (columns.IFAD_LOAN, cf.ifad_loan_item),
    (columns.IFAD_GRANT, cf.ifad_grant_item),
    (columns.GEF, cf.gef_costs_item),
    (columns.ASAP, cf.asap_costs_item),
    (columns.EXP_ACCOUNT, cf.exp_account_item),
    (columns.DISB_ACCOUNT, cf.disb_account_item)
]
)


RECORD_DISPATCHER = {
    "activity": activity_in_record,
    "item": item_in_record
}
