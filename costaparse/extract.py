import os
from costaparse.util import costab as cs
from costaparse.util import column_finders as cf
from costaparse.settings.logging_settings import extractlog
from costaparse.settings import columns, regexes
import json
import glob
import click
import itertools
import pandas as pd
import country_converter as coco
import unidecode

PWD = os.path.join(os.path.dirname(__file__))
COUNTRY_PATH = os.path.join(PWD, "data/country_codes.json")
RATES_PATH = os.path.join(PWD, "data/historical_rates.json")


def ref_col_counts(ref_labels):
    return {k: len(v) for k, v in ref_labels.items()}


def is_float(x):
    try:
        float(x)
        return True
    except ValueError:
        return False


def consecutive_floats(a):
    """ list of strings -> groups of consecutive floats"""
    float_groups = [
        list(map(float, g))
        for k, g in itertools.groupby(a, is_float)
        if k is True
    ]
    return float_groups


def ifad_costs_pos(ref_labels):
    pos = None, None
    exp_fin = ref_labels.get("expenditures by financiers", [None])[0]
    if exp_fin is not None:
        ifads = ref_labels.get("ifad", None)
        totals = ref_labels.get("total", None)
        if ifads is not None and totals is not None:
            ifad = min(ifads, key=lambda x: abs(exp_fin[1] - x[1]))
            pos = next((x for x in totals if x[1] > ifad[1]), (None, None))
    else:
        pos = ref_labels.get("fin. rule", (None, None))
    return pos


def longest_range(a):
    """ TODO check better the meaning of this function"""
    g = consecutive_floats(a)
    if not g:
        return (None, None)
    r = max(g, key=len)
    first, last = min(r), max(r)
    return first, last


def years_in_series(s):
    mm = s.str.lower().str.strip().str.extract(regexes.YEAR_FOUR_TWO)
    fl = mm.dropna(axis=0, how="all").values.flatten()
    four = "20" + pd.Series(fl).dropna().astype(str).str.slice(-2)
    return four.to_list()


def items_from_df(df):
    investment_rows = cs.investmentcosts_rows(df)
    recurrent_rows = cs.recurrentcosts_rows(df)
    ref_labels = cf.reference_columns_join(df)
    #print(ref_labels)
    cols_counter = ref_col_counts(ref_labels)
    records_from_investment = cs.get_records(df, investment_rows, ref_labels)
    records_from_recurrent = cs.get_records(df, recurrent_rows, ref_labels)
    df_records = records_from_investment + records_from_recurrent
    if len(df_records) == 0:
        return []
    totals_row = ref_labels.get(columns.TOTAL, [{}])[-1].get("index", 0)
    years_range = years_in_series(df.loc[totals_row].dropna().astype(str))
    try:
        from_year, to_year = min(years_range), max(years_range)
    except ValueError:
        from_year, to_year = None, None
    subcomponent = cf.subcomponent(df)
    detailed_costs_pos = cf.detailed_costs_pos(df)
    curr_candidates = cf.currency_candidates(
        df, [detailed_costs_pos]
    )
    currency, unit = cf.currency_and_unit_re(curr_candidates)
    common_fields = {
        "subcomponent": subcomponent,
        "sheet_currency": currency,
        "sheet_currency_unit": unit,
        "from_year": from_year,
        "to_year": to_year,
        "num_base_cost": cols_counter.get(columns.BASE_COST, 0),
        "num_contingencies": cols_counter.get(
            columns.CONTINGENCIES, 0),
        "num_unit_cost": cols_counter.get(columns.UNIT_COST, 0)
    }
    invest_jsonified = cs.jsonify_records(
        records_from_investment, cost_type="investment")
    recurrent_jsonified = cs.jsonify_records(
        records_from_recurrent, cost_type="recurrent"
    )
    jsonified = invest_jsonified + recurrent_jsonified
    out = [
        {**common_fields, **j}
        for j in jsonified
    ]
    return out


def extract(costab_file):
    excel = cs.load(costab_file)
    # cost_sheets = cs.cost_sheet_names(excel)
    cost_sheets = excel.sheet_names
    component = cf.component("fake", costab_file)
    proj_number = cf.proj_number(costab_file)
    common_fields = {
        "component": component,
        "project_number": proj_number,
        "file_name": os.path.split(costab_file)[1]
    }
    out = []
    # cost_sheets = ["DT_1"]
    for sheet in cost_sheets:
        extractlog.info("parsing sheet {}".format(sheet))
        df = cs.df_from_name(sheet, excel)
        if len(df) == 0:
            continue
        country_candidates = " ".join(
            [costab_file[len(PWD):], *cf.country_candidates(df)])
        country = coco.convert(unidecode.unidecode(country_candidates))
        items = items_from_df(df)
        component_number = {
            "country": country[-1] if isinstance(country, list) else country,
            "component_number": cf.component_number(sheet),
            "sheet": sheet,
        }
        sheet_records = [
            {**common_fields, **component_number, **item}
            for item in items
        ]
        out += sheet_records
    return out


def update_out(out, records):
    with open(out) as f:
        data = json.load(f)
    data += records
    with open(out, 'w') as f:
        json.dump(data, f)
    return data


@click.command()
@click.option(
    "--path", "-p",
    metavar="INPUT",
    type=click.Path(),
    default="")
@click.option(
    "--out", "-o",
    metavar="OUTPUT",
    type=click.Path(),
    default="out.json")
@click.option(
    "--resume", "-r",
    is_flag=True
)
def main(path, out, resume):
    patterns = ["*.xls*", "*.XLS*"]
    excels = []
    for p in patterns:
        t = os.path.join(PWD, path, "**", p)
        excels += glob.glob(t, recursive=True)
    extractlog.info("extracting from {} files".format(len(excels)))

    # excels = [path]
    if resume:
        try:
            done_costabs = set([x["file_name"] for x in json.load(open(out))])
            excels = [
                x for x in excels if os.path.basename(x) not in done_costabs]
        except FileNotFoundError:
            extractlog.info("nothing to resume from {}".format(out))
            open(out, 'w').write(json.dumps([]))  # outfile init
    else:
        open(out, 'w').write(json.dumps([]))  # outfile init
    for i, excel in enumerate(excels):
        extractlog.info("parsing {i}/{t}: {f}".format(
            i=i+1, t=len(excels), f=excel))
        records = extract(excel)
        if len(records) > 0:
            extractlog.info("extracted {} items".format(len(records)))
        else:
            extractlog.warning("No items found in {}".format(excel))
        extractlog.info("saving to {}".format(out))
        # json.dumps(records)
        data = update_out(out, records)
        extractlog.info("total records saved so far: {}".format(len(data)))
        # print(records)


if __name__ == "__main__":
    main()
