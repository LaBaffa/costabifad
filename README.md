# costabifad

This is a command line program to parse and extract cost information from COSTAB files.

## Install (Linux)

Create and activate a dedicated virtual environment, if you prefer, by doing:

1. install `virtual environment`:
   ``` Bash
   sudo apt install virtualenv
   ```
2. create the environment:

	``` Bash
	virtualenv /path/to/your/environment/
	```
	
3. activate:
   ``` Bash
   source /path/to/your/environment/bin/activate
   ```


Then install the package, (be sure to have `pip` installed), with:

   ```
   pip install -e git+https://gitlab.com/LaBaffa/costabifad.git#egg=costaparse
   ```

## Usage

Put all the COSTAB you want to parse in the same folder, and then:


   ``` Bash
   costaparse --path /your/costab/folder --out /path/to/your_output.json
   ```

This will produce a json file with a list of all items found in the given COSTABs.

Given that the extraction process can take some time, it's also possible to stop the script, and
run it again adding the option `-r` or `--resume`, like this:
	
   ``` Bash
   costaparse --path /your/costab/folder --out /path/to/your_output.json --resume
   ```

This will skip all the files already parsed, saving useful time.


