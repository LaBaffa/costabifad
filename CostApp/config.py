import os
from CostApp import app


DEBUG = True
DATADIR_PATH = os.path.join(app.root_path, "data")
DATASET_FILENAME = "total.xlsx"
DATASET_PATH = os.path.join(DATADIR_PATH, DATASET_FILENAME)
INDEX_DIR = os.path.join(app.root_path, "db_index")
