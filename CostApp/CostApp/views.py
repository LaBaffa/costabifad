import datetime as dt
from CostApp import app, costa_engine
from flask import session, render_template, request, flash


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html', error=None)
    else:
        return render_template(
            'index.html',
            year=dt.datetime.now().year
        )


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if request.form['username'] == 'admin' or \
                request.form['password'] == 'secret':
            session['logged_in'] = True
            flash('You were successfully logged in')
            return home()
    return render_template('login.html')


@app.route('/search')
def search():
    if not session.get('logged_in'):
        return render_template('login.html')
    query = request.args.get("q")
    app.logger.debug("Received query: '{}'".format(query))
    costa_engine.search(query)
    df = costa_engine.search_data()
    out = costa_engine.costs(df)
    out.update(costa_engine.general_info(df))
    columns = list(out.keys())
    items = [out]
    return render_template(
        'index.html',
        year=dt.datetime.now().year,
        columns=columns,
        items=items
    )
