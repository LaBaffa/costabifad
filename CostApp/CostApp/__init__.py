import os
from flask import Flask
from costasearch.engine import Engine


app = Flask(__name__)
app.secret_key = os.urandom(12)
app.config.from_object('config')
costa_engine = Engine(app.config["INDEX_DIR"], app.config["DATASET_PATH"])
from CostApp import views